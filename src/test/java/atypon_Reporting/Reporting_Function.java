package atypon_Reporting;

import java.awt.BorderLayout;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.xml.XMLConstants;
import javax.xml.bind.annotation.XmlID;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.hc.client5.http.entity.mime.FormBodyPartBuilder;
import org.apache.hc.client5.http.impl.auth.HttpAuthenticator;
import org.apache.hc.core5.http.io.HttpTransportMetrics;
import org.apache.poi.ss.usermodel.charts.DataSources;
import org.apache.xmlbeans.XmlDocumentProperties;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.mozilla.javascript.tools.debugger.Dim;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPieChart;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.ResourceCDN;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.graphbuilder.struc.LinkedList.Node;
import com.sun.xml.bind.v2.model.core.Element;
import com.sun.xml.txw2.Document;



import org.w3c.dom.*;

import TestAutomation_Atypon.GlobalVariable;
import TestAutomation_Atypon.App;
import TestAutomation_Atypon.CustomKeywords;
import groovy.util.NodeList;
import jcifs.ntlmssp.NtlmFlags;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException; 
import java.io.Reader; 
import javax.xml.parsers.DocumentBuilder; 
import javax.xml.parsers.DocumentBuilderFactory; 
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import org.apache.axis2.transport.http.*;
import org.apache.commons.codec.*;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileUpload;

// New import for experiment
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
//New import for experiment ends here

import static TestAutomation_Atypon.App.driver;
import static TestAutomation_Atypon.App.driverprovider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Reporting_Function extends App{
	
	CustomKeywords CustomKeywords = new TestAutomation_Atypon.CustomKeywords();
	
	 String browserName;
		
	 @Parameters({ "OS", "browser" })
	    @BeforeTest
	    public void startReport(String OS, String browser) {
	    	
	    	GlobalVariable.browserName = browser;	
	    	browserName = browser;    
	 }
	 
	 @AfterSuite
	  public void aftersuite() throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		 

		 ///////////// office 365 properties //////
	    	// Create object of Property file
			Properties props = new Properties();
	 
			// this will set host of server- you can change based on your requirement 
			props.put("mail.smtp.host", "smtp.office365.com");
	 
			// set the port of socket factory 
			//props.put("mail.smtp.socketFactory.port", "587");
				 
			// set socket factory
			//props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
				 
			// set the authentication to true
			props.put("mail.smtp.auth", "true");
				 
			// set the port of SMTP server
			props.put("mail.smtp.port", "587");
						
			props.put("mail.smtp.starttls.enable", true);
	 
			// This will handle the complete authentication
			Session session = Session.getDefaultInstance(props,
	 
					new javax.mail.Authenticator() {
	 
						protected PasswordAuthentication getPasswordAuthentication() {
	 
							return new PasswordAuthentication("your email goes here", "your password goes here");
	 
						}
	 
					});
	 
			try {
				
				// Create object of MimeMessage class
				Message message = new MimeMessage(session);
	 
				// Set the from address
				message.setFrom(new InternetAddress("set email from here that's the same as your email"));
	 
				// Set the recipient address
				message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("set email recipient here"));
				//////////////////office365 properties end here /////////////////////
				
		
			
		
     
     
				/*//////////// Gmail properties //////
		// Create object of Property file
  			Properties props = new Properties();
  	 
  			// this will set host of server- you can change based on your requirement 
  			props.put("mail.smtp.host", "smtp.gmail.com");
  	 
  			// set the port of socket factory 
  			props.put("mail.smtp.socketFactory.port", "465");
  				 
  			// set socket factory
  			props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
  				 
  			// set the authentication to true
  			props.put("mail.smtp.auth", "true");
  				 
  			// set the port of SMTP server
  			props.put("mail.smtp.port", "465"); //465
  						
  			props.put("mail.smtp.starttls.enable", true);
  	 
  			// This will handle the complete authentication
  			Session session = Session.getDefaultInstance(props,
  	 
  					new javax.mail.Authenticator() {
  	 
  						protected PasswordAuthentication getPasswordAuthentication() {
  	 
  							return new PasswordAuthentication("set your email here", "password goes here");
  	 
  						}
  	 
  					});
  	 
  			try {
  	 
  				// Create object of MimeMessage class
  				Message message = new MimeMessage(session);
  	 
  				// Set the from address
  				message.setFrom(new InternetAddress("set email from here"));
  	 
  				// Set the recipient address
  				message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("set recipient email here"));
  	    
  				////////////////// Gmail properties end here ////////////////////*/
				
				
				// Setting GlobalVariables of Reports' Paths
				
				String userDirectory = System.getProperty("user.dir");
				
				GlobalVariable.Main_Regression_Report_path = CustomKeywords.setting_GBs_suites_pathes(userDirectory+"\\suitesNames\\mainRegression.txt", "mainregression");
				
				// Mention the file which you want to send
				String main_regression = userDirectory + GlobalVariable.Main_Regression_Report_path;
				
				String main_regressionJR = userDirectory + GlobalVariable.Main_Regression_JUnitReport_path;
				
				String reportURL_mainRegression = null;
				
				// Add the subject link
				message.setSubject("Atypon Test Automation Report");
				
				Multipart multipart = new MimeMultipart();
				
				if(!GlobalVariable.Main_Regression_Report_path.equals("null")) {
					
					BodyPart messageBodyPart2 = new MimeBodyPart();					
					DataSource source = new FileDataSource(main_regression);
					messageBodyPart2.setDataHandler(new DataHandler(source));
					 messageBodyPart2.setFileName(main_regression);
						multipart.addBodyPart(messageBodyPart2);
				}
				
				// Create object to add multimedia type content
				BodyPart messageBodyPart = new MimeBodyPart();
				
				
				
				// Collecting suites' results go here
				 
				 String html_fixed_values = "<style>\r\n"
					  		+ "				table, th, td {\r\n"
					  		+ "				  border:1px solid #9F2C83;\r\n"
					  		+ "				}\r\n"
					  		+ "				</style>\r\n"
					  		+ "				<body>\r\n"
					  		+ "\r\n"
					  		+ "				<table style=\"width:50%\">\r\n"
					  		+ "				  <tr>\r\n"
					  		+ "				    <th style= 'color: #9F2C83'>Suite Name</th>\r\n"
					  		//+ "				    <th style= 'color: gray'>Total</th>\r\n"
					  		+ "				    <th style= 'color: green'>Passed</th>\r\n"
					  		+ "				    <th style= 'color: red'>Failed</th>\r\n"
					  		//+ "				    <th style= 'color: #F6C603'>Skipped</th>    \r\n"
					  		+ "				  </tr>\r\n";
				 
				 String html_dynamic_values = " ";
				 
				 if(!GlobalVariable.Main_Regression_Report_path.equals("null")) {
					// reading_junitReports(main_regressionJR);
						reading_HTMLReport(main_regression);
						
						 html_dynamic_values = html_dynamic_values
									+ "				  <tr>\r\n"
							  		+ "				    <td style= 'color: #9F2C83'>Main Regression Suite</td>\r\n"
							  		//+ "				    <td style= 'color: gray'>"+totalTests+"</td>\r\n"
							  		+ "				    <td style= 'color: green'>"+passedTests+"</td>\r\n"
							  		+ "				    <td style= 'color: red'>"+failedTests+"</td>\r\n"
							  		//+ "				    <td style= 'color: #F6C603'>"+skippedTests+"</td>    \r\n"
							  		+ "				  </tr>\r\n"; 
				 }
				 
				 String html_end_of_table  = 
				  		 "				</table>\r\n"
				  		+ "				</body>";
				 
				 
				 
				 messageBodyPart.setContent("<p>Dears,</p>\r\n"+"<p>Here're the Atypon Automation Test results using for your kind perusal.</p>\r\n"+html_fixed_values+html_dynamic_values+html_end_of_table+ "<p>Best regards,</p>"+"<p> Atypon Test Automation Team</p>", "text/html");
					multipart.addBodyPart(messageBodyPart);
					
					// set the content
					message.setContent(multipart);
					
					// finally send the email

					Transport.send(message);
		 
					System.out.println("\n\n"+" ========== EMAIL HAS BEEN GENERATED AND SENT ========== "+"\n\n");
											
		 
				} catch (MessagingException e) {
		 
					throw new RuntimeException(e);
		 
				}
			
	 }
	 
	 static String totalTests;
	 static String passedTests;
	 static String failedTests;
	 static String skippedTests;
	 
	 public static String reading_HTMLReport(String report_Path){
		 
		// Reading HTML file
		 StringBuilder contentBuilder = new StringBuilder();
			try {
			    BufferedReader in = new BufferedReader(new FileReader(report_Path));
			    String str;
			    while ((str = in.readLine()) != null) {
			        contentBuilder.append(str);
			    }
			    in.close();
			} catch (IOException e) {
			}
			String content = contentBuilder.toString();
			
			
			
			 org.jsoup.nodes.Document doc = Jsoup.parse(content);
			JsonObject jsonObject = new JsonObject();
			 JsonArray list = new JsonArray();
			
			 org.jsoup.nodes.Element Script = doc.select("script").get(0);
			 

			 
			 String valueOfPass = Script.toString();
			 
		
			 
			 if (valueOfPass.contains("passParent:")){
				
				 
				 valueOfPass= valueOfPass.split("passParent:")[1];
				 
				  
				 valueOfPass = valueOfPass.replaceFirst(",", "@");
				  
				 valueOfPass = valueOfPass.split("@")[0];
				  
				  System.out.println("valueOfPass is =="+valueOfPass);
				  
				 }
			 
			 
			 String valueOfFail = Script.toString();
			 if (valueOfFail.contains("failParent:")){
				 
				 valueOfFail= valueOfFail.split("failParent:")[1];
				 
				 valueOfFail = valueOfFail.replaceFirst(",", "@");
				  
				 valueOfFail = valueOfFail.split("@")[0];
				  
				  System.out.println("valueOfFail is =="+valueOfFail);
				  			 
			 }
			 
			 
			 /*String valueOfSkip = Script.toString();
			 if (valueOfSkip.contains("skipParent:")){
				//skipParent//exceptionsParent

				 valueOfSkip= valueOfSkip.split("skipParent:")[1];
				 
				 valueOfSkip = valueOfSkip.replaceFirst(",", "@");
				  
				 valueOfSkip = valueOfSkip.split("@")[0];
				  
				  System.out.println("valueOfSkip is =="+valueOfSkip);
			 
			 }*/
			  
			 passedTests = valueOfPass; failedTests = valueOfFail;
			 
			 return "Passed Test Cases: "+valueOfPass + "   / "+"Failed Test Cases: "+valueOfFail;
	 }
	 
	 
	 public static String reading_junitReports(String junitreport_Path)  throws ParserConfigurationException, SAXException, IOException{
			
		 String user_Directory = System.getProperty("user.dir");
		 
		 File xmlFile = new File(junitreport_Path);
		 // Let's get XML file as String using BufferedReader 
		 // FileReader uses platform's default character encoding
		 // if you need to specify a different encoding,
		 // use InputStreamReader
		 Reader fileReader = new FileReader(xmlFile);
		 BufferedReader bufReader = new BufferedReader(fileReader);
		 StringBuilder sb = new StringBuilder();
		 String line = bufReader.readLine();
		 while( line != null){
			 sb.append(line).append("\n"); line = bufReader.readLine();
			 }
		 
		 String xml2String = sb.toString();
		 //System.out.println("XML to String using BufferedReader : ");
		 //System.out.println(xml2String); bufReader.close();
		 
		 String valueOfTests = xml2String;
		 if(valueOfTests.contains("tests=")) {
			 
			 valueOfTests = valueOfTests.split("tests=")[1]; 

			 //System.out.println(valueOfTests);
			 
			 valueOfTests = valueOfTests.replaceFirst("\"", "@"); 
			
			// System.out.println(valueOfTests);
			 
			 valueOfTests = valueOfTests.replaceFirst("@", "").trim();
			 
			 //System.out.println(valueOfTests);
			 
			 valueOfTests = valueOfTests.replaceFirst("\"", "@");
			 
			// System.out.println(valueOfTests);
			 
			 valueOfTests = valueOfTests.split("@")[0];
			 
			 System.out.println("valueOfTests is =="+valueOfTests);
		 }
		 
		 String valueOfFail = xml2String;
		 if(valueOfFail.contains("failures=")) {
			 
			 valueOfFail = valueOfFail.split("failures=")[1];
			 
			 //System.out.println(valueOfFail);
			 
			 valueOfFail = valueOfFail.replaceFirst("\"", "@"); 
			
			 //System.out.println(valueOfFail);
			 
			 valueOfFail = valueOfFail.replaceFirst("@", "").trim();
			 
			 //System.out.println(valueOfFail);
			 
			 valueOfFail = valueOfFail.replaceFirst("\"", "@");
			 
			 //System.out.println(valueOfFail);
			 
			 valueOfFail = valueOfFail.split("@")[0];
			 
			 //System.out.println(valueOfFail);
		 }
		 
		 String valueOfSkip = xml2String;
		 if(valueOfSkip.contains("skipped=")) {
			 
			 valueOfSkip = valueOfSkip.split("skipped=")[1];
			 
			 //System.out.println(valueOfSkip);
			 
			 valueOfSkip = valueOfSkip.replaceFirst("\"", "@"); 
			 
			 //System.out.println(valueOfSkip);
			 
			 valueOfSkip = valueOfSkip.replaceFirst("@", "").trim();
			 
			 //System.out.println(valueOfSkip);
			 
			 valueOfSkip = valueOfSkip.replaceFirst("\"", "@");
			 
			 //System.out.println(valueOfSkip);
			 
			 valueOfSkip = valueOfSkip.split("@")[0];
			 
			 System.out.println("valueOfSkip is =="+valueOfSkip);
		 }
		 
		 totalTests = valueOfTests; skippedTests = valueOfSkip;
		 return "Total Test Cases: "+valueOfTests + "   / "+"Skipped Test Cases: "+valueOfSkip+"   / ";
		 
	  }
	 
}
