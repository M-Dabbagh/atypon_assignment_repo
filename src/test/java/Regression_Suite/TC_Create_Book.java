package Regression_Suite;

import java.io.IOException;
import java.util.ArrayList;

import static TestAutomation_Atypon.App.driver;
import static TestAutomation_Atypon.App.driverprovider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import TestAutomation_Atypon.CustomKeywords;
import TestAutomation_Atypon.GlobalVariable;

import PageObjects.*;

public class TC_Create_Book {
	
	public void createBook(JavascriptExecutor js, WebDriverWait wait) throws InterruptedException, IOException{
		
		CustomKeywords CustomKeywords = new TestAutomation_Atypon.CustomKeywords();
		
		js = (JavascriptExecutor) driver;
				
		// Navigating to localhost
		
		driverprovider().get(CustomKeywords.getApiUrl(GlobalVariable.mainLocalHost));
		
		wait = new WebDriverWait(driver, 10);
		
		Assert.assertTrue(CustomKeywords.waitForVisibility(HomePage.books_nav_sidebar()), "Books button on side bar is not visible");
		
		driver.findElement(HomePage.books_nav_sidebar()).click();
		
		Assert.assertTrue(CustomKeywords.waitForVisibility(BooksPage.create_btn()), "Create Book button is not visible");
		
		driver.findElement(BooksPage.create_btn()).click();
		
		Assert.assertTrue(CustomKeywords.waitForVisibility(BooksPage.title_input()), "Title field does not visible or exist");
		Assert.assertTrue(CustomKeywords.waitForPresence(BooksPage.year_input()), "Year field does not exist even on dom");
		
		//GlobalVariable.book_name = "Book "+CustomKeywords.currenttimestamp();
		GlobalVariable.book_name = "Book "+CustomKeywords.customTimeStamp();
		GlobalVariable.book_year = CustomKeywords.random_year();
		driver.findElement(BooksPage.title_input()).sendKeys(GlobalVariable.book_name);
		
		driver.findElement(BooksPage.year_input()).sendKeys(GlobalVariable.book_year);
		
		driver.findElement(BooksPage.save_button()).click();
		
		// wait for the books list
		
		Assert.assertTrue(CustomKeywords.waitForVisibility(BooksPage.page_header_books()), "Books page header is no visible");
		
		String text_containing_addedBook_id = null;
		
		// Verifying that the added item appears within the books list 
		boolean bookItemGotReallyAdded = false;
		java.util.List<WebElement> bookRecords = new ArrayList();
		Thread.sleep(2000);
		bookRecords = driver.findElements(BooksPage.book_records());
		for(int br = 0; br < bookRecords.size(); br++) {
			if(bookRecords.get(br).findElements(By.cssSelector("td")).get(0).getText().equalsIgnoreCase(GlobalVariable.book_name) ) {
				CustomKeywords.scrollIntoViewBeneath(bookRecords.get(br).findElements(By.cssSelector("td")).get(0));
				bookItemGotReallyAdded = true;
				text_containing_addedBook_id = bookRecords.get(br).findElements(By.cssSelector("a")).get(0).getAttribute("href");
				break;
			}
		}
		Assert.assertTrue(bookItemGotReallyAdded, "The book item does not exist within the books list");
		
		// Getting the book id
		System.out.println("text_containing_addedBook_id is == "+text_containing_addedBook_id);
		GlobalVariable.book_id = text_containing_addedBook_id.split("books/")[1].replace("/edit", "").trim().toString();
		System.out.println("book_id is == "+GlobalVariable.book_id);
		
		// Closing browser
		CustomKeywords.closeBrowser();
				
	}
	
	

}
