package Regression_Suite;

import java.io.IOException;
import java.util.ArrayList;

import static TestAutomation_Atypon.App.driver;
import static TestAutomation_Atypon.App.driverprovider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import TestAutomation_Atypon.CustomKeywords;
import TestAutomation_Atypon.GlobalVariable;

import PageObjects.*;

public class TC_Edit_Book {
	
	public void editBook(JavascriptExecutor js, WebDriverWait wait) throws InterruptedException, IOException{
		
		CustomKeywords CustomKeywords = new TestAutomation_Atypon.CustomKeywords();
		
		js = (JavascriptExecutor) driver;
				
		GlobalVariable.book_Edited_name = GlobalVariable.book_name+" Edited";
		
		// Navigating to localhost
		
		driverprovider().get(CustomKeywords.getApiUrl(GlobalVariable.mainLocalHost));
		
		wait = new WebDriverWait(driver, 10);
		
		Assert.assertTrue(CustomKeywords.waitForVisibility(HomePage.books_nav_sidebar()), "Books button on side bar is not visible");
		
		driver.findElement(HomePage.books_nav_sidebar()).click();
		
		Assert.assertTrue(CustomKeywords.waitForVisibility(BooksPage.create_btn()), "Create Book button is not visible");
		
		
		
		// Clicking on Edit button of the added book from the previous TC
		java.util.List<WebElement> bookRecords = new ArrayList();
		Thread.sleep(2000);
		bookRecords = driver.findElements(BooksPage.book_records());
		for(int br = 0; br < bookRecords.size(); br++) {
			if(bookRecords.get(br).findElements(By.cssSelector("td")).get(0).getText().equalsIgnoreCase(GlobalVariable.book_name) ) {
				CustomKeywords.scrollIntoViewBeneath(bookRecords.get(br).findElements(By.cssSelector("td")).get(0));
				bookRecords.get(br).findElements(By.cssSelector("a")).get(0).click();
				break;
			}
		}
		
	Assert.assertTrue(CustomKeywords.waitForVisibility(BooksPage.title_input()), "book page did not open");
	
	driver.findElement(BooksPage.title_input()).clear();
	Thread.sleep(500);
	driver.findElement(BooksPage.title_input()).sendKeys(GlobalVariable.book_Edited_name);
	
	driver.findElement(BooksPage.save_button()).click();
	
	Assert.assertTrue(CustomKeywords.waitForVisibility(BooksPage.page_header_books()), "Saving book sis not get performed");
	
	
	
	 // Verifying that the oldname does not  appear anymore within the books list 
		boolean bookItemoldOldnameAppears = false;
		java.util.List<WebElement> bookRecordsOld = new ArrayList();
		Thread.sleep(2000);
		bookRecordsOld = driver.findElements(BooksPage.book_records());
		for(int brOld = 0; brOld < bookRecordsOld.size(); brOld++) {
			if(bookRecordsOld.get(brOld).findElements(By.cssSelector("td")).get(0).getText().equalsIgnoreCase(GlobalVariable.book_name) ) {
				CustomKeywords.scrollIntoViewBeneath(bookRecordsOld.get(brOld).findElements(By.cssSelector("td")).get(0));
				bookItemoldOldnameAppears = true;
				break;
			}
		}
		Assert.assertFalse(bookItemoldOldnameAppears, "The old book name still appears within the list");
	 
	
	// Verifying that the edited item appears within the books list with the right changes 
			boolean bookItemGotReallyEdited = false;
			java.util.List<WebElement> bookRecords2 = new ArrayList();
			Thread.sleep(2000);
			bookRecords2 = driver.findElements(BooksPage.book_records());
			for(int br2 = 0; br2 < bookRecords2.size(); br2++) {
				if(bookRecords2.get(br2).findElements(By.cssSelector("td")).get(0).getText().equalsIgnoreCase(GlobalVariable.book_Edited_name) ) {
					bookItemGotReallyEdited = true;
					break;
				}
			}
			Assert.assertTrue(bookItemGotReallyEdited, "The book editing did not get the new name");
			
			// Closing browser
			CustomKeywords.closeBrowser();

	}

}
