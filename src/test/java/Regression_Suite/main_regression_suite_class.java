package Regression_Suite;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.ResourceCDN;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;

import TestAutomation_Atypon.CustomKeywords;
import TestAutomation_Atypon.GlobalVariable;

public class main_regression_suite_class extends TestAutomation_Atypon.App{
	
	CustomKeywords CustomKeywords = new CustomKeywords();
	
	 String ReportName_with_path;
		
	    ExtentHtmlReporter htmlReporter;
	    ExtentReports extent;
	    
	    //helps to generate the logs in test report.
	    ExtentTest test;
	    
	    // Report code //
	    
	    String browserName;
	    
	    @Parameters({"OS", "browser", "environment", "credentials"})
	    @BeforeTest
	    public void startReport(String OS, String browser, String environment, String credentials) throws IOException, InterruptedException {
	    	
	    	GlobalVariable.browserName = browser;	
	    	browserName = browser;  
	    	
	    	GlobalVariable.env = environment;
	    	GlobalVariable.cred = credentials;
	    	//CustomKeywords.getUsername();
	    	//CustomKeywords.getPassword();
	    	
	    		    	
	    	String reportRename = CustomKeywords.getRandomString(16);
	    	ReportName_with_path = "/test-output/mainRegressionSuite"+reportRename+".html";
	    	
	    	// initialize the HtmlReporter
	    	//htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +"/test-output/RESTAssured_Strength_Not_Divisible.html");
	    	htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +ReportName_with_path);
	    	
	    	//initialize ExtentReports and attach the HtmlReporter
	        extent = new ExtentReports();
	        extent.attachReporter(htmlReporter);
	        
	      //To add system or environment info by using the setSystemInfo method.
	        extent.setSystemInfo("OS", OS);
	        extent.setSystemInfo("Browser", browser);
	        
	      //configuration items to change the look and feel
	        //add content, manage tests etc
	        htmlReporter.config().setChartVisibilityOnOpen(true);
	        htmlReporter.config().setDocumentTitle("Atypon Test Automation");
	        htmlReporter.config().setReportName("Main Regression Suite");  // To be changed to the suite name
	        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
	        htmlReporter.config().setTheme(Theme.STANDARD);
	        htmlReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");
	        
	        htmlReporter.config().getTestViewChartLocation();
	        htmlReporter.config().setCSS("css-string");
	        htmlReporter.config().setEncoding("utf-8");
	        htmlReporter.config().setJS("js-string");
	        htmlReporter.config().setProtocol(Protocol.HTTPS);
	        
	        htmlReporter.config().setCSS(".node.level-1  ul{ display:none;} .node.level-1.active ul{display:block;}  .card-panel.environment  th:first-child{ width:30%;}");
	        htmlReporter.config().setJS("$(window).off(\"keydown\");");
	        htmlReporter.config().setResourceCDN(ResourceCDN.EXTENTREPORTS);
	        
	    }
	    
	  //This method is to capture the screenshot and return the path of the screenshot.
	    public static String getScreenhot(String screenshotName) throws Exception {
	   		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
	   		TakesScreenshot ts = (TakesScreenshot) driver;
	   		File source = ts.getScreenshotAs(OutputType.FILE);
	                   //after execution, you could see a folder "FailedTestsScreenshots" under src folder
	   		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/"+screenshotName+dateName+".png";
	   		File finalDestination = new File(destination);
	   		FileUtils.copyFile(source, finalDestination);
	   		return destination;
	   	}
	    
	    
	    

	    //////////////////////Creating objects for the test cases./////////////////////////
	    
	    TC_Create_Book createbook = new TC_Create_Book();
	    
	    TC_API_Get_Book apieditbook = new TC_API_Get_Book();
	    
	    TC_Edit_Book editbook = new TC_Edit_Book();
	    
	    TC_Delete_Book deletebook = new TC_Delete_Book(); 
	    	    

	    /////////////////// Test cases list starts here///////////////////////////////////
	    
	    
	    @Test (priority = 0)
	    public void createbooktest() throws InterruptedException, IOException{
	    	test = extent.createTest("Create Book Item", "PASSED test case"); //Test Case name to be appeared on the report.
	    	createbook.createBook(js, wait);
	    }
	    
	     @Test (priority = 1)
	    public void apieditbooktest() throws InterruptedException, IOException{
	    	test = extent.createTest("Get Book Details API", "PASSED test case"); //Test Case name to be appeared on the report.
	    	apieditbook.getBookAPI();
	    }
	    
	    @Test (priority = 2)
	    public void editbooktest() throws InterruptedException, IOException{
	    	test = extent.createTest("Edit Book Item", "PASSED test case"); //Test Case name to be appeared on the report.
	    	editbook.editBook(js, wait);
	    }
	    
	    @Test (priority = 3)
	    public void deletebooktest() throws InterruptedException, IOException{
	    	test = extent.createTest("Delete Book Item", "PASSED test case"); //Test Case name to be appeared on the report.
	    	deletebook.deleteBook(js, wait);
	    }
	    
	        
	    
	    @AfterMethod	
	    public void getResult(ITestResult result) throws Throwable {
	    	
	    	String RequestBody = "Request Body\n"+GlobalVariable.requestBody;
	    	String responseLabel = "Response Body\n"+GlobalVariable.Consol;
	    	
	        if(result.getStatus() == ITestResult.FAILURE) {
	            test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" FAILED ", ExtentColor.RED));
	            test.fail(result.getThrowable());
	            //String screenshotPath = getScreenhot(result.getName());
	            
	            if(GlobalVariable.requestBody != null) {
	            	test.fail(MarkupHelper.createCodeBlock(RequestBody));
	            }
	            
	            if(GlobalVariable.Consol != null) {
	            	test.fail(MarkupHelper.createCodeBlock(responseLabel));
	            }
	     
	          //To add it in the extent report 
				//test.addScreenCaptureFromPath(screenshotPath);
	        }
	        else if(result.getStatus() == ITestResult.SUCCESS) {
	            test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" PASSED ", ExtentColor.GREEN));
	            
	            if(GlobalVariable.requestBody != null) {
	            	test.pass(MarkupHelper.createCodeBlock(RequestBody));
	            }
	            
	            if(GlobalVariable.Consol != null) {
	            	test.pass(MarkupHelper.createCodeBlock(responseLabel));
	            }
	            
	        }
	        else {
	            test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" SKIPPED ", ExtentColor.ORANGE));
	            test.skip(result.getThrowable());
	        }
	        
	        
	        // Resetting the APIs GB values
	        GlobalVariable.requestBody = null;
	        GlobalVariable.Consol = null;
	    }
	    
	    
	    
	    @AfterTest
	    public void tearDown() {
	    	//to write or update test information to reporter
	        extent.flush();
	   
	    }
	    
	    
	    @AfterSuite
	    public void aftersuite() throws IOException {
	    	
	    	
	    	GlobalVariable.Main_Regression_Report_path = ReportName_with_path;
	    	
	    	// to be changed
	    	GlobalVariable.Main_Regression_JUnitReport_path = "\\target\\surefire-reports\\junitreports\\TEST-Regression_Suite.main_regression_suite_class.xml";
	    	
	    	// Calling the method which replacing the null value by the path value
	    	String userDirectory = System.getProperty("user.dir");
	    	
	    	CustomKeywords.replacing_all_suites_names_value(userDirectory+"\\suitesNames\\mainregression.txt", "mainregression", ReportName_with_path);
	    	
		}
	        
	    
	    

}
