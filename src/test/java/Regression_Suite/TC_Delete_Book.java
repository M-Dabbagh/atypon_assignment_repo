package Regression_Suite;

import java.io.IOException;
import java.util.ArrayList;

import static TestAutomation_Atypon.App.driver;
import static TestAutomation_Atypon.App.driverprovider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import TestAutomation_Atypon.CustomKeywords;
import TestAutomation_Atypon.GlobalVariable;

import PageObjects.*;

public class TC_Delete_Book {
	
	public void deleteBook(JavascriptExecutor js, WebDriverWait wait) throws InterruptedException, IOException{
		
		CustomKeywords CustomKeywords = new TestAutomation_Atypon.CustomKeywords();
		
		js = (JavascriptExecutor) driver;
		
		// Navigating to localhost
		driverprovider().get(CustomKeywords.getApiUrl(GlobalVariable.mainLocalHost));
						
		wait = new WebDriverWait(driver, 10);
						
		Assert.assertTrue(CustomKeywords.waitForVisibility(HomePage.books_nav_sidebar()), "Books button on side bar is not visible");
						
		driver.findElement(HomePage.books_nav_sidebar()).click();
				
		Assert.assertTrue(CustomKeywords.waitForVisibility(BooksPage.create_btn()), "Create Book button is not visible");
		
		// Clicking on Delete button of the added book from the previous TC
		
		java.util.List<WebElement> bookRecords = new ArrayList();
		Thread.sleep(2000);
		bookRecords = driver.findElements(BooksPage.book_records());
		for(int br = 0; br < bookRecords.size(); br++) {
			if((bookRecords.get(br).findElements(By.cssSelector("td")).get(0).getText().equalsIgnoreCase(GlobalVariable.book_name)) || (bookRecords.get(br).findElements(By.cssSelector("td")).get(0).getText().equalsIgnoreCase(GlobalVariable.book_Edited_name))) {
				CustomKeywords.scrollIntoViewBeneath(bookRecords.get(br).findElements(By.cssSelector("td")).get(0));
				bookRecords.get(br).findElements(By.cssSelector("a")).get(1).click();
				break;	
			}
			
		}
		
		Thread.sleep(1000);
		
		// Verifying that the item got really deleted and it does not appear within the list
		boolean item_is_deleted = true;
		java.util.List<WebElement> bookRecords_after_delete = new ArrayList();
		Thread.sleep(2000);
		for(int br_after_delete = 0; br_after_delete < bookRecords_after_delete.size(); br_after_delete++) {
			if((bookRecords_after_delete.get(br_after_delete).findElements(By.cssSelector("td")).get(0).getText().equalsIgnoreCase(GlobalVariable.book_name)) || (bookRecords.get(br_after_delete).findElements(By.cssSelector("td")).get(0).getText().equalsIgnoreCase(GlobalVariable.book_Edited_name))) {
				item_is_deleted = false;
				break;
			}
			
		}
		Assert.assertTrue(item_is_deleted, "Item is not deleted and it's still appears within the list");
		
		// Closing browser
		CustomKeywords.closeBrowser();
		
	}
	
	

}
