package Regression_Suite;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.testng.Assert;

import TestAutomation_Atypon.GlobalVariable;
import TestAutomation_Atypon.CustomKeywords;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class TC_API_Get_Book {
	
	public  void getBookAPI() throws IOException, InterruptedException{
		
		CustomKeywords CustomKeywords = new CustomKeywords();
		
		GlobalVariable.Consol = null;
		
		Response response =
				given()
			       .when()
			       .get("http://localhost:3306/api/books/"+GlobalVariable.book_id+"")
			       .then()
			       .log()
			       .all()
			       .contentType(ContentType.JSON)
			       .extract().response();
		
		GlobalVariable.Consol = response.jsonPath().prettify();
		
		Assert.assertEquals(response.getStatusCode(), 200, "header status code is not 200");
		
		Assert.assertEquals(response.jsonPath().getString("title"), GlobalVariable.book_name, "title tag does not retrieve the correct book name value");
		Assert.assertEquals(response.jsonPath().getString("year"), GlobalVariable.book_year, "year tag does not retrieve the correct book year value");
				
	}
	
}
