package AtyponTest_Preconditions;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.testng.annotations.AfterSuite;
import org.xml.sax.SAXException;

import TestAutomation_Atypon.CustomKeywords;

public class Suites_Preconditions {
	
	@AfterSuite
	  public void aftersuite() throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		
		CustomKeywords CustomKeywords = new TestAutomation_Atypon.CustomKeywords();
		
		String userDirectory = System.getProperty("user.dir");
		
		CustomKeywords.resetting_all_suites_names(userDirectory+"\\suitesNames\\mainregression.txt", "mainregression");
		
		
	}

}
