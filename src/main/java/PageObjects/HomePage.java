package PageObjects;

import org.openqa.selenium.By;

public class HomePage {
	
	private static By selector = null;
	
	public static By books_nav_sidebar() {
		selector = (By.cssSelector(".nav-sidebar li a[href='/books']"));
		return selector;
	}

}
