package PageObjects;

import org.openqa.selenium.By;

public class BooksPage {
	
	private static By selector = null;
	
	public static By create_btn() {
		selector = (By.xpath("//a[text()='Create Book']"));
		return selector;
	}
	
	public static By title_input() {
		selector = (By.id("title"));
		return selector;
	}

	public static By year_input() {
		selector = (By.id("year"));
		return selector;
	}
	
	public static By save_button() {
		selector = (By.cssSelector("button[type='submit'].btn-success"));
		return selector;
	}
	
	public static By page_header_books() {
		selector = (By.xpath("//h1[@class='page-header'][text()='Books']"));
		return selector;
	}
	
	public static By book_records() {
		selector = (By.cssSelector("table tbody tr"));
		return selector;
	}
	
	public static By Book_Records() {
		selector = (By.xpath("//table//tbody//tr"));
		return selector;
	}
	
	
}
