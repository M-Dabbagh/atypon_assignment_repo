package TestAutomation_Atypon;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.firefox.FirefoxDriver;

public class firefoxDriver {
	
	public static WebDriver driverprovider() {
		
	WebDriver firefoxdriver;
		
	WebDriverManager.firefoxdriver().setup();

	firefoxdriver = new FirefoxDriver();
	firefoxdriver.manage().window().maximize();
		
	App.driver = firefoxdriver;
		
	return firefoxdriver;
		
	}
}
