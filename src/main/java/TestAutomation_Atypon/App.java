package TestAutomation_Atypon;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import TestAutomation_Atypon.chromeDriver;
import TestAutomation_Atypon.edgeDriver;
import TestAutomation_Atypon.firefoxDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import TestAutomation_Atypon.App;

import TestAutomation_Atypon.GlobalVariable;

public class App {
	
	public static WebDriver driver;
	public JavascriptExecutor js;
	public Actions action;
	public WebDriverWait wait;
	
	 
		 
	chromeDriver chromedriver = new chromeDriver();
	edgeDriver edgedriver = new edgeDriver();
	firefoxDriver firefoxdriver = new firefoxDriver();

    
	
	
	public static WebDriver driverprovider() {
		switch(GlobalVariable.browserName) {
		case"Chrome":
			driver = chromedriver();
			break;
			
		case"Edge":
			driver = edgedriver();
			break;
				
		case"Firefox":
			driver = firefoxdriver();			
			break;
		
		}	
		return driver;
	}
	
	
	private static WebDriver chromedriver() {
		return chromeDriver.driverprovider();
	}
	
	private static WebDriver edgedriver() {
		return edgeDriver.driverprovider();
	}
	
	private static WebDriver firefoxdriver() {
		return firefoxDriver.driverprovider();
	}
	
    
}
