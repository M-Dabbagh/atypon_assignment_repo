package TestAutomation_Atypon;

public class GlobalVariable {
	
	public static String browserName = null;
	
	public static String env = null;
	
	public static String cred = null;
	
	public static String book_name = null;
	
	public static String book_Edited_name = null;
	
	public static String book_year = null;
	
	public static String author_name = null;
	
	public static String Main_Regression_Report_path = "null";
	
	public static String Main_Regression_JUnitReport_path = "\\target\\surefire-reports\\junitreports\\TEST-Regression_Suite.main_regression_suite_class.xml";
	
	public static String mainLocalHost = "mainlocalhost";
	
	public static String apiUrl = null;
	
	public static String Consol = null;
	
	public static String requestBody = null;
	
	public static String book_id = null;
	
	
}
