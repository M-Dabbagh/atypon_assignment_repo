package TestAutomation_Atypon;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;

public class edgeDriver {
	
public static WebDriver driverprovider() {
		
		WebDriver driver;
		
		WebDriverManager.edgedriver().setup();
		//EdgeOptions options = new EdgeOptions();
		
				 
		//edgedriver = new EdgeDriver(options);
		//edgedriver.manage().window().maximize();
		
		//edgedriver = new EdgeDriver(options);
		driver = new EdgeDriver();
		driver.manage().window().maximize();
		
		App.driver = driver;
		
		return driver;
		
	}

}
