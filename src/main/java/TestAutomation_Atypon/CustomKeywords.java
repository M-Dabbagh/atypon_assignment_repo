package TestAutomation_Atypon;

import static TestAutomation_Atypon.App.driver;

import static TestAutomation_Atypon.App.driverprovider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.google.common.base.Function;

import java.sql.RowIdLifetime;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.nio.charset.*;
import java.util.*;

public class CustomKeywords extends App{
	
	public String theuser;
	public String thepass;
	public String thesenderid;
	public String theurl;
	public String theenv;
	public boolean found=false;
	public String themodule1;
	public String myclient;
	public String why;
	public String userDirectory = System.getProperty("user.dir");
	public int rowIndexCounter;
	

	//////////////////////////////Fluent Waiting Methods //////////////////////////////
	
	// Fluent wait for element to be displayed 
	public void fluentWaitIsDisplayed(final By by) {
	    FluentWait<WebDriver> wait2 = new FluentWait<WebDriver>(driver)
	            .withTimeout(30, TimeUnit.SECONDS)
	            .pollingEvery(5, TimeUnit.SECONDS)
	            .ignoring(NoSuchElementException.class);
	            
	            WebElement element = wait2.until(new Function<WebDriver, WebElement>() {
	            
	            public WebElement apply(WebDriver driver) {
	            WebElement element = driver.findElement(by);
	            if(element.isDisplayed()){ 
	            return element;
	            }else{
	            System.out.println("FluentWait Is Displayed Failed");
	            return null;
	            }
	           }
	          });
	     }
	
	// Fluent wait for element to be enabled 
	public void fluentWaitIsEnabled(final By by) {
		FluentWait<WebDriver> wait2 = new FluentWait<WebDriver>(driver)
	    .withTimeout(30, TimeUnit.SECONDS)
	    .pollingEvery(5, TimeUnit.SECONDS)
	    .ignoring(NoSuchElementException.class);
	    
	    WebElement groupNameInput = wait2.until(new Function<WebDriver, WebElement>() {
	    public WebElement apply(WebDriver driver) {
	    WebElement groupNameInput = driver.findElement(by);
	    int i = 0;
	    if(groupNameInput.isEnabled()){
	    return groupNameInput;
	    }else{
	    System.out.println("FluentWait IS Enabled Failed"+i++);
	    return null;
	    }
	   }
	  });
	}
	
	// Fluent wait for element to get a text displayed on it 
	public void fluentWaitIsTextAppearOnElement(final By by, final String text) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
	        .withTimeout(30, TimeUnit.SECONDS)
	        .pollingEvery(5, TimeUnit.SECONDS)
	        .ignoring(NoSuchElementException.class);
	        
	        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
	        public WebElement apply(WebDriver driver) {
	        
	        WebElement element = driver.findElement(by);
	        
	        String getTextOnPage = element.getText();
	        
	        if(getTextOnPage.equals(text)){
	        System.out.println(getTextOnPage);
	        return element;
	        }else{
	        System.out.println("FluentWait Failed");
	        return null;      
	        }
	        }
	       });
		}
	
	// Fluent wait for a specific CSS value to get displayed on an element
	public void fluentWaitIsCSSValueAppearOnElement(final By by, final String cSSValue, final String desiredValue) {
		FluentWait<WebDriver> wait2 = new FluentWait<WebDriver>(driver)
	    	    .withTimeout(30, TimeUnit.SECONDS)
	    	    .pollingEvery(5, TimeUnit.SECONDS)
	    	    .ignoring(NoSuchElementException.class);
	    	    
	    	    WebElement groupNameInput = wait2.until(new Function<WebDriver, WebElement>() {
	    	    public WebElement apply(WebDriver driver) {
	    	    WebElement groupNameInput = driver.findElement(by);
	    	    if(groupNameInput.getCssValue(cSSValue).equalsIgnoreCase(desiredValue)){
	    	    return groupNameInput;
	    	    }else{
	    	    System.out.println("FluentWait Failed");
	    	    return null;
	    	    }
	    	   }
	    	  });
		}
	
	//////////////////////////////Fluent Waiting Methods End Here //////////////////////////////
	
	
	
	// Closing browser in case it was still open
	public void closeBrowser () {
		try {
			driver.close();
		}catch(Throwable t) {
			t.printStackTrace();
		}
	}
	
	

	//////////////////////////////Waiting Methods //////////////////////////////
	
	// wait for element to be present
	public boolean waitForPresence(By by) {
		wait = new WebDriverWait(driver, 10);
		boolean elementIsPresent = false;
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(by));
			elementIsPresent = true;
		}catch(Throwable t) {
			t.printStackTrace();
		}
		return elementIsPresent;
	}
	
	// wait for element to be visible
	public boolean waitForVisibility(By by) {
		wait = new WebDriverWait(driver, 10);
		boolean elementIsVisible = false;
		try {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
			elementIsVisible = true;
		}catch(Throwable t) {
			t.printStackTrace();
		}
		
		return elementIsVisible;
	}
	
	// wait for element to be invisible
	public boolean waitForInVisibility(By by) {
		wait = new WebDriverWait(driver, 10);
		boolean elementIsInVisible = false;
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
			elementIsInVisible = true;
		}catch(Throwable t) {
			t.printStackTrace();
		}
		
		return elementIsInVisible;
	}
	
	// wait for element to be clickable
	public boolean waitForElementClickable(By by) {
		wait = new WebDriverWait(driver, 10);
		boolean elementIsclickable = false;
		try {
			wait.until(ExpectedConditions.elementToBeClickable(by));
			elementIsclickable = true;
		}catch(Throwable t) {
			t.printStackTrace();
		}
		return elementIsclickable;
	}
	

	//////////////////////////////Waiting Methods End Here //////////////////////////////
	
	// Generating a random string consisted of chars only from A-Z whether upper or lower case
	public String getRandomString(int i) {
 	   // bind the length 
        //bytearray = new byte[256]; 
 	   byte[] bytearray = new byte[256];
        String mystring;
        StringBuffer thebuffer;
        String theAlphaNumericS;

        new Random().nextBytes(bytearray); 

        mystring 
            = new String(bytearray, Charset.forName("UTF-8")); 
            
        thebuffer = new StringBuffer();
        
        //remove all spacial char 
        theAlphaNumericS = mystring.replaceAll("[^A-Za-z]", ""); 

        //random selection
        for (int m = 0; m < theAlphaNumericS.length(); m++) { 

            if (Character.isLetter(theAlphaNumericS.charAt(m)) 
                    && (i > 0) 
                || Character.isDigit(theAlphaNumericS.charAt(m)) 
                    && (i > 0)) { 

                thebuffer.append(theAlphaNumericS.charAt(m));
                i--; 
            } 
        } 

        // the resulting string 
         
        return thebuffer.toString(); 
    }
	
	

	//////////////////////////////////////For Emails //////////////////////////////////////
	
	
	public static void resetting_all_suites_names(String fileName,String text) throws IOException {
 	   
 	   ////////////////////////
		   // Here's the code of increasing the counter for choosing a different row each run
		   // The counter has a public variable located on the top of this class and its value will be taken from txt file
		   String oldValue="";
		   String  oldtext ="";
		   String  suite_value="";		
		   
		   
		   String value="";
  	   Path path = Paths.get(fileName);
  	   Scanner scanner = new Scanner(path,"UTF-8");
 		
  	   //process each line
 		
  	   while(scanner.hasNextLine()){
  		   String line = scanner.nextLine();
  		   
  		   if(line.contains(text)){
  			   value=line.trim().split(":")[1];
  			  suite_value= value.replace(value, "null");
  			   break;   
  		   }   
  	   }
  	   scanner.close();
  	   
  	  try
			{
				BufferedReader reader = new BufferedReader(new FileReader(fileName));
				String line = "";
				if((line = reader.readLine()) != null && line.contains(text))
				{
					oldtext += line + "\r\n";
				}
				reader.close();

				//To replace a line in a file
				oldValue=line.split(":")[1];

				System.out.println("oldValue  is  ==  "+oldValue);
				
				
				String newtext = oldtext.replace(oldValue+"", suite_value+"");
				System.out.println("newtext is  ==  "+newtext); //I added it
				FileWriter writer = new FileWriter(fileName);
				writer.write(newtext);
				writer.close();
			

			
			}
			catch (IOException ioe)
			{
				ioe.printStackTrace();
			}
  	   
    }
	
	
	public static void replacing_all_suites_names_value(String fileName,String text, String suitePAth) throws IOException {
 	   
 	   String oldValue="";
		   String  oldtext ="";
		   String  suite_value= "";		
		   
		   
		   String value="";
		   Path path = Paths.get(fileName);
  	   Scanner scanner = new Scanner(path,"UTF-8");
 		
  	   //process each line
 		
  	   while(scanner.hasNextLine()){
  		   String line = scanner.nextLine();
  		   
  		   if(line.contains(text)){
  			   value=line.trim().split(":")[1];
  			   if(value.equals("null")) {
  				  suite_value= value.replace(value, suitePAth);
	     			   break;
  			   }
  			    
  		   }   
  	   }
  	   scanner.close();
  	   
  	  try
			{
				BufferedReader reader = new BufferedReader(new FileReader(fileName));
				String line = "";
				if((line = reader.readLine()) != null && line.contains(text))
				{
					oldtext += line + "\r\n";
				}
				reader.close();

				//To replace a line in a file
				oldValue=line.split(":")[1];

				System.out.println("oldValue  is  ==  "+oldValue);
				
				
				String newtext = oldtext.replace(oldValue+"", suite_value+"");
				System.out.println("newtext is  ==  "+newtext); //I added it
				FileWriter writer = new FileWriter(fileName);
				writer.write(newtext);
				writer.close();
			

			
			}
			catch (IOException ioe)
			{
				ioe.printStackTrace();
			}
 	   
    }
	
	
	
	public static String setting_GBs_suites_pathes(String fileName,String text) throws IOException {
 	   
 	   ////////////////////////
		   // Here's the code of increasing the counter for choosing a different row each run
		   // The counter has a public variable located on the top of this class and its value will be taken from txt file
				   
		   String value="";
  	   Path path = Paths.get(fileName);
  	   Scanner scanner = new Scanner(path,"UTF-8");
 		
  	   //process each line
 		
  	   while(scanner.hasNextLine()){
  		   String line = scanner.nextLine();
  		   
  		   if(line.contains(text)){
  			  value=line.trim().split(":")[1];
  			   break;   
  		   }   
  	   }
  	   scanner.close();
  	   
  	   return value;
}
	
	
	// Getting the API's Url 
	public String getApiUrl(String apiName) throws IOException, InterruptedException {
		
		xlsxReadingForUrls(apiName);
		return theurl;
		
	}
	
	
	//////////////Reading from xlsx for getting the urls //////////////////
	public void xlsxReadingForUrls(String APIName) throws IOException, InterruptedException  {
 	   
 	   String userDirectory = System.getProperty("user.dir");
 	   String filePath = userDirectory + "\\users and urls\\credentials and urls for APIs.xlsx";
 	   
 	   FileInputStream fileXlsx = new FileInputStream(filePath);
 	   
 	   boolean rowfound = false;
 	   
 	   String url = null;
 	

 	   XSSFWorkbook workbook = new XSSFWorkbook(fileXlsx);  	   
 		   
	    		   
 		   switch(GlobalVariable.env) {
 			case"localhost":
 				 XSSFSheet sheet = workbook.getSheet("Localhost URLs");	
 				
 				 for(int r = 0; r < sheet.getPhysicalNumberOfRows(); r++) {
		    			   
		    			  
		    			   
		    			   XSSFRow row = sheet.getRow(r);
		    			   for(int c = 0; c < row.getPhysicalNumberOfCells(); c++) {
		    				   			  
		    				   
		    				   XSSFCell cell = row.getCell(c);
		    				   
		    				   String  cellValue =null; 
		
		    				   cell.setCellType(CellType.STRING);
		
		    				   cellValue = cell.getStringCellValue();
		    				   
		    				   // Checking for desired row
		    				   if(cellValue.equalsIgnoreCase(APIName)) {
		    					   			    					   
		    					   rowfound = true;
		    					   
		    					   row.getCell(1).setCellType(CellType.STRING);
		    					   url = row.getCell(1).getStringCellValue();
		    					   theurl = url;
		    					   System.out.println("url value is:  "+url);	    
		    					   
		    				   }
		    				// Checking for desired row ends here
		    				       				   			    				   
		    			   }
		    			   
		    			   if (rowfound == true) {
		    			   break;
		    			   }
		    		   
	    			   
	    		   }
 				 
 				 
 				 break;
 				 
 		   }
	}
	
	 ////////////////////////////// Generating Random Values Methods //////////////////////////////
	
	public Timestamp currenttimestamp() {
 	   
 	   Date date= new Date();
 	   long time = date.getTime();
 	   Timestamp ts = new Timestamp(time);  
 	   
 	   return ts;
 	   
    }
	
	public String customTimeStamp() {
    	
   	 Date date= new Date();
   	 long time = date.getTime();
   	 Timestamp ts = new Timestamp(time); 
   	 SimpleDateFormat formatter = new SimpleDateFormat("DDMMYYYYhhmmssms");
   	 
   	 return formatter.format(ts);
   }
	
	public static String randomNumber(int n) {
	    int m = (int) Math.pow(10, n - 1);
	    
	    int random = m + new Random().nextInt(9 * m);
	    //return m + new Random().nextInt(9 * m);
	    
	    String value = Integer.toString(random);
	    return value;
	    
	}
	
	public static String random_year() {
		int value = (int) (Math.floor(Math.random() * (2021 - 1900 + 1) ) + 1900);
		
		String string_value = Integer.toString(value);
		
		return string_value;
	}
	
	

	//////////////////////////////Scrolling Methods //////////////////////////////
	
	// Scroll into element view that is above where you are
    public void scrollIntoViewAbove(By element) {
 	   js = (JavascriptExecutor) driver;
 	   WebElement scrollInto = driver.findElement(element);
 	   js.executeScript("arguments[0].scrollIntoView(false);", scrollInto);
    }
	
    
	// Scroll into element view that is beneath where you are
    public void scrollIntoViewBeneath(WebElement element) {
 	   js = (JavascriptExecutor) driver;
 	   WebElement scrollInto = element;
 	   js.executeScript("arguments[0].scrollIntoView(true);", scrollInto);
    }
	
	

}
