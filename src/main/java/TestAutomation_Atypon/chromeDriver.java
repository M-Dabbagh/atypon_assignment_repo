package TestAutomation_Atypon;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import TestAutomation_Atypon.App;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;

public class chromeDriver {
	
	public static WebDriver driverprovider() {
		 
		 WebDriver driver;
	     WebDriverManager.chromedriver().setup();
	     ChromeOptions options = new ChromeOptions();
	     options.addArguments("start-maximized");
	     options.addArguments("enable-automation");
	     
	     driver = new ChromeDriver(options);  
	     	
	     App.driver = driver;
	     
		return driver;
		 
	 }
	
	

}
