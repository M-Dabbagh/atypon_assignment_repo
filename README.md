Hello,

The framework incubates 4 test cases within the regression suite, the test caes are: create a book, then there's an api test case to get the added book details and verify them, then edit the added book and finally deleting the book along with verifying the actions.

This framework is configured to generate email containing the test results along with the report attachment upon finishing the test execution, but in order to get it done you only have to put your email credentials in the following path, it's for the intended reporting function:

src/test/java/atypon_Reporting/Reporting_Function.java 

This function handles gmail and office365 so you have to comment the undesired one and put your credials within the desired one.

Then you're happy to go...

To run the test you only have to open the cmd from project directory then just run the follwoing command mvn test

Thanks,